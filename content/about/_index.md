---
title: "About"
date: 2020-11-01T06:34:24-06:00
authorImage: "images/compass_v2.png"
---

Hello! I am Jonas, a graduate from the University of Birmingham in BSc Computer Science.
I enjoy learning about graphics, computer architecture, game development and programming languages. This can be seen in my portfolio and my experience in game jams.

I am currently looking for employment.

