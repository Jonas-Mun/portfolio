+++
categories = ["documentation", "mathematics", "algorithms"]
description = "Mathematics and Algorithms to implement a Grid formation behaviour"
image = "/portfolio/images/math_algo.png"
title = "Grid Formation: Mathematics and Algorithms"
+++

I go over the mathematics and algorithms need to implement a *Grid Formation*, the likes of Total War. 
Utilizing the Unit Circle to be able to drag units around and maintain a formation
based on any angle.

You can check the documentation <a href="https://jonas-mun.gitlab.io/blog/tutorials/grid_formation/grid_formation/">here</a>.
