+++
categories = ["game-jam","game-dev", "unity"]
description = "Card Game made for the Historiccally Accurate game jam"
image = "/portfolio/images/docplague.png"
title = "Doctor and Plague"
[[tech]]
logo = "/portfolio/images/unity_logo.png"
name = "Unity"
url = "https://unity.com"
+++

I joined a team of five to create a card game for the **Historically Accurate Two** game jam that lasted one week.

{{< vimeo  474057998 >}}

It was made by a team of five people:
* Jonas - Lead Programmer
* D.Menari - Game Designer
* Viktor - Artist
* Eperson - 3D Modeller & Programmer

We teamed up to create a 3D card game set during the black plague. Everything was created
from scratch. I was in charge of implementing the rules and card systems that allow
the game to be played. This includes all the logic and game transitions to produce a
smooth gameplay.

You can check my <a href="https://jonas-mun.gitlab.io/blog/articles/game_jams/doctor_plague/">Devlog</a> or a discussion of the <a href="https://jonas-mun.gitlab.io/blog/articles/software_arch/doctor_plague_improv_arch/">Software Architecture.</a>

Check out the <a href="https://singularity-games.itch.io/doctor-plague">game</a> on itch.io.

