+++
categories = ["software","rust","ray-trace"]
description = "Ray tracer made in Rust"
image = "/portfolio/images/rust_rt.png"
title = "Rust Ray Tracer"
[[tech]]
logo = "/portfolio/images/rust_logo.png"
name = "Rust"
url = "https://www.rust-lang.org/"
+++

Followed the tutorial <a href="https://raytracing.github.io/books/RayTracingInOneWeekend.html"> </a> and implemented the ray tracer in rust.
It was a good opportunity to learn rust as well as ray tracing.

<a href="https://gitlab.com/Jonas-Mun/ray_tracer_rust">Source Code</a>
