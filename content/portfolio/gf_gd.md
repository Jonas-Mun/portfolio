+++
categories = ["software","godot","implementation"]
description = "Implemented the Grid Formation behaviour in the Godot Game Engine"
image = "/portfolio/images/gf_gd.png"
title = "Grid Formation in Godot"
[[tech]]
logo = "/portfolio/images/godot_logo.png"
name = "Godot"
url = "https://godotengine.org"
+++

I managed to implement the **Grid Formation: Mathematics & Algorithms** in the Godot game engine as a module.

<a href="https://gitlab.com/Jonas-Mun/grid_formation_godot_module">Source Code</a>

It now includes 3D Spherical coordinates.
