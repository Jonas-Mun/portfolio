+++
categories = ["game-jam", "game-dev", "unity"]
description = "Puzzle game with the theme of ducks"
image = "/portfolio/images/flock_ducks.png"
title = "Flock of Ducks"
[[tech]]
logo = "/portfolio/images/godot_logo.png"
name = "Godot"
url = "https://godotengine.org/"
+++

Made a game in a team of two for the **Godot Wild Jam**

It was made by the following people:
* Jonas - Lead Programmer & UI Developer
* Xicoh_84 - Music
* GrapedFruitChili - Audio SFX & Level Designer

The game was about a flock of ducks waddling through puzzles to solve in order to reach their destination.
We had one week to create the game, and although we did no implement the story, we were able to come up with a playable game for the game jam.

Check out my <a href="https://jonas-mun.gitlab.io/blog/articles/software_arch/3d_architecture/">Devlog</a> about the architecture of the game.

You can find the game <a href="https://jonas1426.itch.io/fluck-of-ducks">here</a>.
